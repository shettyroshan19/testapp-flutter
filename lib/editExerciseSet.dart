import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:test_app_flutter/classScore.dart';
import 'package:test_app_flutter/classes.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/style.dart';
import 'package:test_app_flutter/writeTest.dart';

import 'exerciseSets.dart';
import 'exercises.dart';
import 'generated/l10n.dart';
import 'globals.dart' as globals;

class EditExerciseSet extends StatefulWidget {
  final int id;
  final Map testData;

  EditExerciseSet({this.id, this.testData});

  @override
  _EditExerciseSetState createState() => _EditExerciseSetState();
}

class _EditExerciseSetState extends State<EditExerciseSet> {
  bool _dataAvailable = false;
  bool _newTest = false;
  TextEditingController _testNameController = TextEditingController();
  ExercisesSelection _selectedExercises = ExercisesSelection();
  ScrollController _scrollController = ScrollController();
  bool _savingTest = false;
  bool _loadWriteTest = false;
  bool _loadAssignTest = false;
  Map _testData;

  @override
  void initState() {
    if (widget.testData == null && widget.id != null) {
      globals.api
          .call('listJoin', options: {'showAll': true}, context: context)
          .then((data) {
        if (data['response'] is List) {
          List _joinTests = data['response'];
          _testData = _joinTests.firstWhere((elem) => (elem.id == widget.id),
              orElse: () {
            Navigator.of(context).pop();
          });
          setState(() {
            init();
          });
        }
      });
    } else {
      _testData = widget.testData;
      init();
    }
    super.initState();
  }

  void init() {
    if (widget.id != null) {
      _selectedExercises.addAll(jsonDecode(_testData['exercises']));
      _testNameController.text = _testData['name'];
    } else
      _newTest = true;
    setState(() {
      _dataAvailable = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return (ResponsiveDrawerScaffold(
      helpPage: HelpPage(
          path: HelpPagePath.editJoinTest,
          label: S.of(context).helpEditingJoinTests),
      appBarLeading: IconButton(
          icon: FaIcon(FontAwesomeIcons.times),
          onPressed: Navigator.of(context).pop),
      appBarActions: (!_newTest)
          ? <Widget>[
              IconButton(
                icon: FaIcon(
                  FontAwesomeIcons.clone,
                  size: 18.0,
                ),
                onPressed: () {
                  _newTest = true;
                },
                tooltip: S.of(context).forkThisTest,
              ),
              IconButton(
                icon: FaIcon(FontAwesomeIcons.trash),
                onPressed: deleteTest,
                tooltip: S.of(context).deleteThisJoinTest,
              )
            ]
          : null,
      title: S.of(context).editJoinTest,
      body: (_dataAvailable)
          ? TestAppScrollBar(
              controller: _scrollController,
              child: ListView(
                controller: _scrollController,
                children: <Widget>[
                  TestAppCard(
                    children: <Widget>[
                      Text(
                        S.of(context).joinTestName,
                        style: Theme.of(context).textTheme.headline6,
                      ),
                      TextField(
                        controller: _testNameController,
                        decoration: InputDecoration(
                            labelText: S.of(context).joinTestName),
                      )
                    ],
                  ),
                  TestAppCard(
                    children: <Widget>[
                      Text(
                        S.of(context).exercises,
                        style: Theme.of(context).textTheme.headline6,
                      ),
                      if (_dataAvailable)
                        Text(
                            "${_selectedExercises.toSet().length.toString()} " +
                                S.of(context).exercisesSelected),
                      (_dataAvailable)
                          ? Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: OutlinedButton.icon(
                                  icon: FaIcon(FontAwesomeIcons.graduationCap),
                                  onPressed: selectExercises,
                                  label: Text(S.of(context).pickExercises)),
                            )
                          : CenterProgress()
                    ],
                  ),
                  if (widget.id != null)
                    TestAppCard(
                      children: <Widget>[
                        Text(
                          S.of(context).writeTest,
                          style: Theme.of(context).textTheme.headline6,
                        ),
                        (_dataAvailable)
                            ? Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: ButtonBar(children: [
                                  OutlinedButton.icon(
                                      icon: (!_loadWriteTest)
                                          ? FaIcon(FontAwesomeIcons.penNib)
                                          : Container(
                                              height: 18,
                                              width: 18,
                                              child: CircularProgressIndicator(
                                                valueColor:
                                                    AlwaysStoppedAnimation(
                                                        Theme.of(context)
                                                            .accentColor),
                                              ),
                                            ),
                                      onPressed: writeJoinTest,
                                      label: Text(S.of(context).writeThisTest)),
                                  OutlinedButton.icon(
                                      icon: (!_loadAssignTest)
                                          ? FaIcon(FontAwesomeIcons.check)
                                          : Container(
                                              height: 18,
                                              width: 18,
                                              child: CircularProgressIndicator(
                                                valueColor:
                                                    AlwaysStoppedAnimation(
                                                        Theme.of(context)
                                                            .accentColor),
                                              ),
                                            ),
                                      onPressed: assignTest,
                                      label: Text(
                                          S.of(context).useAsAssignment +
                                              '...')),
                                ]),
                              )
                            : CenterProgress()
                      ],
                    ),
                ],
              ),
            )
          : CenterProgress(),
      floatingActionButton: (!_savingTest)
          ? FloatingActionButton.extended(
              onPressed: saveTest,
              label: Text(S.of(context).saveJoinTest),
              icon: FaIcon(FontAwesomeIcons.check),
            )
          : FloatingActionButton(
              onPressed: () {},
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Colors.white),
              ),
            ),
    ));
  }

  void selectExercises() async {
    ExercisesSelection exercises = await Navigator.of(context)
        .push(new MaterialPageRoute<ExercisesSelection>(
            builder: (BuildContext context) {
              return Exercises(
                  selectExercises: true, selectedExercises: _selectedExercises);
            },
            fullscreenDialog: true));
    if (exercises != null) setState(() => _selectedExercises = exercises);
  }

  Future<void> saveTest() async {
    if (_testNameController.text.trim() != '' &&
        _selectedExercises.toSet().length != 0) {
      setState(() {
        _savingTest = true;
      });
      Map<String, dynamic> options = {
        'exercises': _selectedExercises.toSet().toList(),
        'name': _testNameController.text.trim()
      };
      if (!_newTest) options['id'] = widget.id;
      String command = (!_newTest) ? 'updateJoin' : 'addJoin';
      globals.api
          .call(command, options: options, context: context)
          .then((data) {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (c) => ExerciseSets()));
      });
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(S.of(context).pleaseFillInAllFields)));
    }
  }

  void deleteTest() {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              content: Text(S.of(context).areYouSureToDeleteThisJoinTest),
              actions: [
                MaterialButton(
                  onPressed: Navigator.of(context).pop,
                  child: Text('Cancel'),
                ),
                MaterialButton(
                    onPressed: () async {
                      setState(() {
                        _savingTest = true;
                      });
                      globals.api
                          .call('deleteJoin',
                              options: {'id': widget.id}, context: context)
                          .then((data) {
                        Navigator.of(context).push(
                            MaterialPageRoute(builder: (b) => ExerciseSets()));
                      });
                    },
                    child: Text(S.of(context).yes)),
              ],
            ));
  }

  void writeJoinTest() async {
    setState(() {
      _loadWriteTest = true;
    });
    TextEditingController _testTimeController =
        TextEditingController(text: '10');
    TextEditingController _testExpiryController =
        TextEditingController(text: '30');

    Map response = await globals.api
        .call('listClasses', options: {'showAll': false}, context: context);
    List classes = response['response'];

    int _selectedClass;

    Widget classesDropdown = Row(children: [
      Text(S.of(context).wordClass + ': '),
      DropdownButton<int>(
        value: _selectedClass,
        icon: FaIcon(FontAwesomeIcons.users),
        iconSize: 24,
        elevation: 16,
        //itemHeight: 48,
        style: TextStyle(color: Colors.lightBlue),
        underline: Container(
          height: 2,
          color: Colors.green,
        ),
        onChanged: (int newValue) {
          setState(() {
            _selectedClass = newValue;
          });
        },
        items: classes.map((currentClass) {
          return DropdownMenuItem<int>(
            value: int.parse(currentClass['id']),
            child: Text(
              currentClass['name'],
              style: Theme.of(context).textTheme.bodyText1,
            ),
          );
        }).toList(),
      )
    ]);

    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              content: Column(
                children: <Widget>[
                  TextField(
                    controller: _testTimeController,
                    decoration: InputDecoration(
                        labelText: S.of(context).testTimeInMinutes),
                    keyboardType: TextInputType.number,
                  ),
                  TextField(
                    controller: _testExpiryController,
                    decoration: InputDecoration(
                        labelText:
                            S.of(context).allowStartingTestingForInMinutes),
                    keyboardType: TextInputType.number,
                  ),
                  classesDropdown,
                ],
              ),
              actions: <Widget>[
                MaterialButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text(S.of(context).cancel),
                ),
                MaterialButton(
                  onPressed: () {
                    setState(() {
                      _loadWriteTest = false;
                    });
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (b) => WriteJoinTestDialog(
                            id: widget.id,
                            time: int.parse(_testTimeController.text),
                            expiry: int.parse(_testExpiryController.text),
                            testClass: _selectedClass)));
                  },
                  child: Text(S.of(context).startTest),
                )
              ],
            )).then((value) => setState(() => _loadWriteTest = false));
  }

  void assignTest() async {
    setState(() {
      _loadAssignTest = true;
    });
    TextEditingController _testExpiryController =
        TextEditingController(text: '7');
    TextEditingController _assignmentNameController = TextEditingController();
    TextEditingController _assignmentDescriptionController =
        TextEditingController();

    Map response = await globals.api
        .call('listClasses', options: {'showAll': false}, context: context);
    if (!(response['response'] is List) ||
        (response['response'] as List).isEmpty) showNoClassesDialog();
    List classes = response['response'];

    int _selectedClass;

    Widget classesDropdown = Row(children: [
      Text(S.of(context).wordClass + ': '),
      DropdownButton<int>(
        value: _selectedClass,
        icon: FaIcon(FontAwesomeIcons.users),
        iconSize: 24,
        elevation: 16,
        //itemHeight: 48,
        style: TextStyle(color: Colors.lightBlue),
        underline: Container(
          height: 2,
          color: Colors.green,
        ),
        onChanged: (int newValue) {
          setState(() {
            _selectedClass = newValue;
          });
        },
        items: classes.map((currentClass) {
          return DropdownMenuItem<int>(
            value: int.parse(currentClass['id']),
            child: Text(
              currentClass['name'],
              style: Theme.of(context).textTheme.bodyText1,
            ),
          );
        }).toList(),
      )
    ]);

    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              content: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    TextField(
                      controller: _assignmentNameController,
                      decoration: InputDecoration(
                          labelText: S.of(context).assignmentTitle),
                    ),
                    TextField(
                      controller: _assignmentDescriptionController,
                      decoration: InputDecoration(
                          labelText: S.of(context).assignmentContent),
                      maxLines: null,
                    ),
                    TextField(
                      controller: _testExpiryController,
                      decoration: InputDecoration(
                          labelText: S.of(context).timeToWorkOn,
                          helperText: S.of(context).inDays),
                      keyboardType: TextInputType.number,
                    ),
                    classesDropdown,
                  ],
                ),
              ),
              actions: <Widget>[
                MaterialButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text(S.of(context).cancel),
                ),
                MaterialButton(
                  onPressed: () async {
                    setState(() {
                      _loadAssignTest = false;
                    });
                    int expiry = (DateTime.now().millisecondsSinceEpoch / 1000 +
                            double.parse(_testExpiryController.text
                                .replaceAll(',', '.')))
                        .round();
                    await globals.api.call('addAssignment',
                        options: {
                          'class': _selectedClass,
                          'id': widget.id,
                          'describtion': _assignmentDescriptionController.text,
                          'name': _assignmentNameController.text,
                          'expiry': expiry
                        },
                        context: context);
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                      builder: (c) => ClassScore(
                          classId: _selectedClass,
                          initialPage: ClassScoreTab.Assignments),
                    ));
                  },
                  child: Text(S.of(context).createAssignment),
                )
              ],
            )).then((value) => setState(() => _loadAssignTest = false));
  }

  void showNoClassesDialog() {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: Text(S.of(context).youDidNotCreateAnyClassYet),
              content: Text(S.of(context).ifYouWantToUseAnExerciseSetYouNeed),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                      setState(() => _loadAssignTest = false);
                    },
                    child: Text(S.of(context).cancel)),
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pushReplacement(
                          MaterialPageRoute(builder: (context) => Classes()));
                      setState(() => _loadAssignTest = false);
                    },
                    child: Text(S.of(context).createClass)),
              ],
            ));
  }
}

class WriteJoinTestDialog extends StatefulWidget {
  final int id, time, expiry, testClass;

  WriteJoinTestDialog(
      {@required this.id,
      @required this.testClass,
      @required this.time,
      @required this.expiry});

  @override
  _WriteJoinTestDialogState createState() => _WriteJoinTestDialogState();
}

class _WriteJoinTestDialogState extends State<WriteJoinTestDialog> {
  bool _testActivated = false;
  Map _testData;

  bool _timerStarted = false;
  Widget _timer;

  @override
  void initState() {
    globals.api
        .call('activateJoinTest',
            options: {
              'id': widget.id,
              'class': widget.testClass,
              'time': widget.time,
              'expiry': widget.expiry
            },
            context: context)
        .then((data) {
      setState(() {
        _testData = data['response'];
        _testActivated = true;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return (ResponsiveDrawerScaffold(
      helpPage: HelpPage(
          path: HelpPagePath.writeJoinTest,
          label: S.of(context).helpWitingJoinTests),
      title: S.of(context).writeJoinTest,
      appBarLeading: IconButton(
        icon: FaIcon(FontAwesomeIcons.times),
        onPressed: () => Navigator.of(context)
            .push(MaterialPageRoute(builder: (b) => ExerciseSets())),
        tooltip: S.of(context).close,
      ),
      body: (_testActivated)
          ? Column(
              children: <Widget>[
                TestAppCard(
                  children: <Widget>[
                    Text(
                      S.of(context).info,
                      style: Theme.of(context).textTheme.headline6,
                    ),
                    ListTile(
                      title: Text(S.of(context).processingTime +
                          ": ${(int.parse(_testData['writingTime'].toString()) / 60).toString()} " +
                          S.of(context).minutes),
                      leading: FaIcon(FontAwesomeIcons.stopwatch),
                      subtitle: Wrap(
                        children: (_timerStarted)
                            ? [
                                Text(S.of(context).timeLeft + ': '),
                                _timer,
                              ]
                            : [
                                OutlinedButton(
                                  onPressed: () {
                                    setState(() {
                                      _timerStarted = true;
                                      _timer = JoinTestTimer(
                                        time: int.parse(_testData['writingTime']
                                            .toString()),
                                        color: Colors.black,
                                      );
                                    });
                                  },
                                  child: Text(S.of(context).startPersonalTimer),
                                )
                              ],
                      ),
                    ),
                    Divider(),
                    ListTile(
                      title: Text(S.of(context).joinId + ": "),
                      leading: FaIcon(FontAwesomeIcons.hashtag),
                      isThreeLine: true,
                      subtitle: Center(
                        child: Text(
                          _testData['id'].toString(),
                          style: Theme.of(context).textTheme.headline1,
                        ),
                      ),
                    ),
                    Text(S.of(context).pleaseShareThisIdToYourStudents),
                    Divider(),
                    ListTile(
                      title: OutlinedButton(
                          child: Text(
                              S.of(context).finishedWritingTakeMeToClassScore),
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (b) =>
                                    ClassScore(classId: widget.testClass)));
                          }),
                      leading: FaIcon(FontAwesomeIcons.users),
                    )
                  ],
                )
              ],
            )
          : CenterProgress(
              label: S.of(context).activatingTest,
            ),
    ));
  }
}
