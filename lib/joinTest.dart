import 'package:flutter/material.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/style.dart';
import 'package:test_app_flutter/writeTest.dart';

import 'generated/l10n.dart';
import 'globals.dart' as globals;

class JoinTest extends StatefulWidget {
  JoinTest({Key key, exercises}) : super(key: key);

  @override
  _JoinTestState createState() => _JoinTestState();
}

class _JoinTestState extends State<JoinTest> {
  var api = globals.api;
  int _joinId;
  bool _showSnackBar = false;
  ScrollController _scrollController = ScrollController();

  Future<void> _startTest() async {
    Map<String, dynamic> options = Map();
    options['id'] = _joinId;
    options['images'] = true;
    api.call('joinTest', options: options, context: context).then(((data) {
      if (data['response'] == false) {
        setState(() {
          _showSnackBar = true;
        });
      } else {
        var time = data['response']['joinTestTime'];
        data['response'].remove('joinTestTime');
        //Converting map to list
        List exercises = [];
        data['response'].keys.toList().forEach((key) {
          exercises.add(data['response'][key]);
        });
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => WriteTest(
                  exercises: exercises,
                  joinId: _joinId,
                  testTime: int.parse(time))),
        );
      }
    }));
    setState(() {
      _showSnackBar = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveDrawerScaffold(
      helpPage: HelpPage(
          path: HelpPagePath.joinTest, label: S.of(context).helpWithJoinTests),
      body: TestAppScrollBar(
        controller: _scrollController,
        child: SingleChildScrollView(
            controller: _scrollController,
            child: TestAppCard(children: <Widget>[
              Text(
                S.of(context).joinTest,
                style: Theme.of(context).textTheme.headline6,
              ),
              TextField(
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    labelText: S.of(context).joinid,
                    helperText:
                        S.of(context).ifYouDontHaveAJoinidPleaseAskYourTeacher),
                onChanged: (newValue) {
                  setState(() {
                    _joinId = int.parse(newValue);
                  });
                },
              ),
              ElevatedButton(
                child: Text(S.of(context).startTest),
                onPressed: () => _startTest(),
              ),
              if (_showSnackBar)
                Text(
                  S.of(context).ooopsThisJoinidIsNotValidForYou,
                  style: TextStyle(color: Colors.deepOrange[300]),
                ),
            ])),
      ),
    );
  }
}
