/*
import 'dart:convert';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import 'api.dart';
import 'generated/l10n.dart';

class TestAppPush {
  static Future<bool> requestPermission() async {
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();
    return await flutterLocalNotificationsPlugin.initialize(
        InitializationSettings(
            AndroidInitializationSettings('ic_launcher_foreground'),
            IOSInitializationSettings(
              requestSoundPermission: false,
              requestBadgePermission: true,
              requestAlertPermission: false,
              //onDidReceiveLocalNotification: onDidReceiveLocalNotification,
            )),
        onSelectNotification: selectNotification);
  }

  static Future selectNotification(String payload) async {
    if (payload != null) {
      print('notification payload: ' + payload);
    }
  }

  static updateSchedule({@required BuildContext context, Time time}) async {
    if (time == null) time = Time(17, 0, 0);

    String subject;
    try {
      List subjects =
          jsonDecode(await Preferences().fetch('knownSubjectNames'));

      if (subjects.isEmpty) {
        subject = S.of(context).stuff;
      } else {
        subject = subjects[Random().nextInt(subjects.length)];
      }
    } catch (e) {
      subject = S.of(context).stuff;
    }

    print('Showing next push for $subject.');

    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();

    flutterLocalNotificationsPlugin.cancelAll();

    await flutterLocalNotificationsPlugin.showDailyAtTime(
        0,
        S.of(context).testappDailyPractice,
        S.of(context).timeToPracticeSome + subject + S.of(context).today,
        // Note: contains U+1F389 (🎉) code point at the end
        time,
        NotificationDetails(
            AndroidNotificationDetails(
                'daily_reminder',
                S.of(context).dailyPracticeNotification,
                S.of(context).aMotivatingMessageToKeepYouPracticing),
            IOSNotificationDetails()));
  }

  static unsubscribe() {
    FlutterLocalNotificationsPlugin().cancelAll();
  }

  static Future<bool> isSubscribed() async {
    return (await FlutterLocalNotificationsPlugin()
            .pendingNotificationRequests())
        .isNotEmpty;
  }
}
*/
