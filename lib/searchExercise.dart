import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/globals.dart';
import 'package:test_app_flutter/style.dart';
import 'package:test_app_flutter/writeTest.dart';

import 'editExercise.dart';
import 'exerciseSolution.dart';
import 'generated/l10n.dart';

class SearchExercise extends StatefulWidget {
  @override
  _SearchExerciseState createState() => _SearchExerciseState();
}

class _SearchExerciseState extends State<SearchExercise> {
  ScrollController _scrollController = ScrollController();

  TextEditingController _searchQueryController = TextEditingController();

  String _searchQuery = '';

  bool _searchTermTooShort = true;
  bool _loading = true;

  List _matchingExercises = [];
  bool _errorLoadingExercises = false;

  int _exerciseLoading = -1;

  @override
  void initState() {
    _searchQueryController.addListener(_updateSearchQuery);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveDrawerScaffold(
      helpPage: HelpPage(
          label: S.of(context).helpWithPractiseTests,
          path: HelpPagePath.randomTest),
      title: S.of(context).searchExercises,
      appBarLeading: IconButton(
          icon: FaIcon(FontAwesomeIcons.times),
          onPressed: () => Navigator.of(context).pop()),
      body: Stack(
        children: [
          Padding(
              padding: EdgeInsets.only(top: 100),
              child: (_searchTermTooShort)
                  ? ListTile(
                      leading: FaIcon(
                        FontAwesomeIcons.info,
                        color: Theme.of(context).accentColor,
                      ),
                      title: Text(S.of(context).pleaseEnterASearchTerm),
                    )
                  : _loading
                      ? CenterProgress()
                      : _errorLoadingExercises
                          ? ListTile(
                              leading: FaIcon(
                                FontAwesomeIcons.infoCircle,
                                color: Colors.deepOrange,
                              ),
                              title:
                                  Text(S.of(context).noMatchingExercisesFound),
                            )
                          : TestAppScrollBar(
                              controller: _scrollController,
                              child: ListView.builder(
                                controller: _scrollController,
                                itemBuilder: (context, i) {
                                  final Map exercise = _matchingExercises[i];
                                  return Hero(
                                    tag: 'ExerciseBox' +
                                        exercise['id'].toString(),
                                    child: TestAppCard(children: [
                                      Text(
                                        exercise['name'],
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline6,
                                      ),
                                      Builder(
                                          builder: (context) =>
                                              TestAppTex(exercise['content'])),
                                      if (exercise['img'] != null &&
                                          exercise['img'] != "")
                                        Container(
                                            child: Image.memory(
                                          Uri.parse(exercise['img'])
                                              .data
                                              .contentAsBytes(),
                                          scale: 2,
                                        )),
                                      ButtonBar(
                                        children: [
                                          if (isAdmin)
                                            TextButton(
                                              onPressed: () =>
                                                  Navigator.of(context).push(
                                                      MaterialPageRoute(
                                                          builder:
                                                              (c) =>
                                                                  EditExercise(
                                                                    id: int.parse(
                                                                        exercise['id']
                                                                            .toString()),
                                                                  ))),
                                              child: Text(
                                                  S.of(context).editExercise),
                                            ),
                                          TextButton(
                                              onPressed: () async {
                                                setState(() {
                                                  _exerciseLoading = int.parse(
                                                      exercise['id']
                                                          .toString());
                                                });
                                                api
                                                    .call('fetchExercises',
                                                        options: {
                                                          'exercise':
                                                              exercise['id']
                                                        },
                                                        context: context)
                                                    .then((response) {
                                                  setState(() {
                                                    _exerciseLoading = -1;
                                                  });
                                                  Navigator.of(context).push(
                                                      MaterialPageRoute(
                                                          builder: (c) =>
                                                              ExerciseSolution(
                                                                  exercise:
                                                                      response[
                                                                              'response']
                                                                          [
                                                                          0])));
                                                });
                                              },
                                              child: _exerciseLoading ==
                                                      int.parse(exercise['id']
                                                          .toString())
                                                  ? CircularProgressIndicator(
                                                      valueColor:
                                                          AlwaysStoppedAnimation(
                                                              Theme.of(context)
                                                                  .backgroundColor),
                                                    )
                                                  : Text(S
                                                      .of(context)
                                                      .viewFullExercise))
                                        ],
                                      )
                                    ]),
                                  );
                                },
                                itemCount: _matchingExercises.length,
                              ),
                            )),
          Card(
            child: ListTile(
                trailing: IconButton(
                  icon: FaIcon(FontAwesomeIcons.times),
                  tooltip: S.of(context).clear,
                  onPressed: () => _searchQueryController.clear(),
                ),
                leading: IconButton(
                  icon: FaIcon(FontAwesomeIcons.search),
                  onPressed: () {
                    FocusScopeNode currentFocus = FocusScope.of(context);
                    if (!currentFocus.hasPrimaryFocus) currentFocus.unfocus();
                  },
                ),
                title: TextField(
                  maxLines: 1,
                  style: TextStyle(fontSize: 24),
                  autofocus: true,
                  controller: _searchQueryController,
                  decoration: InputDecoration(border: InputBorder.none),
                )),
          ),
        ],
      ),
    );
  }

  Future<void> _updateSearchQuery() async {
    if (_searchQuery == _searchQueryController.text) return;
    _searchQuery = _searchQueryController.text;
    if (_searchQuery.length <= 3) {
      if (true != _searchTermTooShort)
        setState(() => _searchTermTooShort = true);
    } else {
      if (false != _searchTermTooShort)
        setState(() => _searchTermTooShort = false);
    }
    setState(() => _loading = true);
    api
        .call('fetchExercises',
            options: {'query': _searchQuery, 'images': true}, context: context)
        .then((response) {
      if (response['response'] is List) {
        _errorLoadingExercises = false;
        _matchingExercises = response['response'];
      } else {
        _errorLoadingExercises = true;
      }
      setState(() => _loading = false);
    });
  }
}
